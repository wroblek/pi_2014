<?php 
  include('checklogin.php');
  if(isset($_POST['name']))
    $_SESSION['name'] = $_POST['name'];
  if(isset($_POST['lastname']))
    $_SESSION['lastname'] = $_POST['lastname'];
  if(isset($_POST['birthday']))
    $_SESSION['birthday'] = $_POST['birthday'];
  if(isset($_POST['age']))
    $_SESSION['age'] = $_POST['age'];
  if(isset($_POST['pesel']))
    $_SESSION['pesel'] = $_POST['pesel'];
  if(isset($_POST['sex']))
    $_SESSION['sex'] = $_POST['sex'];
  if(isset($_POST['faculty']))
    $_SESSION['faculty'] = $_POST['faculty'];
  if(isset($_POST['comment']))
    $_SESSION['comment'] = $_POST['comment'];
  if(isset($_POST['agreement']))
    $_SESSION['agreement'] = $_POST['agreement'];
  if(isset($_POST['posted']))
    $_SESSION['posted'] = '1';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Strona domowa Krzysztofa Wróblewskiego">
  <meta name="author" content="Krzysztof Wróblewski">
  <meta http-equiv="content-language" content="pl">
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link rel="stylesheet" type="text/css" href="form.css">
  <title>Strona domowa Krzysztofa Wrólewskiego</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <div class="center-text">
        <h2>Strona domowa Krzysztofa Wrólewskiego</h2>
      </div>
    </div>
    <div id="menu">
      <h3>Menu</h3>
      <ul>
        <li>
          <div class="menu-1-level"><a href="/~wroblek1/index.html">Strona główna</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 1</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/frame.html">Ramki</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/iframe.html">Pływająca ramka</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.html">CV (div,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.html">CV (table,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.xhtml">CV (div,xhtml)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.xhtml">CV(table,xhtml)</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 2</a>
          </div>         
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/table.html">Dynamiczna tabela</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/form.html">Formularz</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 3</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/index.php">Strona główna</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/form.php">Formularz</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/logout.php">Wyloguj</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 4</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 5</a>
          </div>
        </li>
      </ul>
    </div>
    <div id="content">
      <div class="entry">
        <div class="entry-header">
          <h4>Dane</h4>
        </div>
        <div class="entry-content no-indent">  
            <div class="row">
              <a href="form-edit.php">[Edytuj]</a>
            </div>        
            <div class="row">
              <span>Imię: </span>
              <span><?php echo $_SESSION['name']; ?></span>
            </div>

            <div class="row">
              <span>Nazwisko: </span>
              <span><?php echo $_SESSION['lastname']; ?></span>
            </div>

            <div class="row">
              <span>Data urodzenia: </span>
              <span><?php echo $_SESSION['birthday']; ?></span>
            </div>

            <div class="row">
              <span>Wiek: </span>
              <span><?php echo $_SESSION['age']; ?></span>
            </div>

            <div class="row">
              <span>PESEL: </span>
              <span><?php echo $_SESSION['pesel']; ?></span>
            </div>

            <div class="row">
              <span>Płeć: </span>
              <span><?php echo $_SESSION['sex'] ?></span>
            </div>
            <div class="row">
              <span>Kierunek studiów: </span>
              <span><?php echo $_SESSION['faculty']; ?></span>
            </div>

            <div class="row">
              <span>Komentarz: </span>
              <span><?php echo $_SESSION['comment'];?></span>
            </div>
        </div>
      </div>
    </div>

    <div id="footer">
      <div id="page-source">
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p3/form.php">Źródło strony</a>
        <a> | </a>
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p3/form.css">Źródło styli</a>
      </div>
      <div id="copyright">&copy; Krzysztof Wróblewski</div>
      <div id="logo-w3">
        <a href="http://validator.w3.org/check?uri=referer">
          <img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="31" width="88">
        </a>
        <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wroblek1%2Fstyle.css">
          <img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Poprawny CSS!">
        </a>
      </div>
    </div>
  </div>
  <div id="prog-int">
    Programowanie internetowe
  </div>
</body>

</html>
