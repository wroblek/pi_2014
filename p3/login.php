<?php
  session_start();
  if(isset($_POST['login']) && $_POST['login'] == 'student' && isset($_POST['pass']) && $_POST['pass'] == 'zet'){
    $_SESSION['logged'] = '1';
    $_SESSION['name'] = '';
    $_SESSION['lastname'] = '';
    $_SESSION['birthday'] = '';
    $_SESSION['age'] = '';
    $_SESSION['pesel'] = '';
    $_SESSION['sex'] = '';
    $_SESSION['faculty'] = '';
    $_SESSION['comment'] = '';
    $_SESSION['agreement'] = '';
    $_SESSION['posted'] = '';

    header("Location: index.php");
  }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Strona domowa Krzysztofa Wróblewskiego">
  <meta name="author" content="Krzysztof Wróblewski">
  <meta http-equiv="content-language" content="pl">
  <script type="text/javascript" src="form.js"></script>
  <link rel="stylesheet" type="text/css" href="../style.css">
  <link rel="stylesheet" type="text/css" href="form.css">
  <title>Strona domowa Krzysztofa Wrólewskiego</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <div class="center-text">
        <h2>Strona domowa Krzysztofa Wrólewskiego</h2>
      </div>
    </div>
    <div id="menu">
      <h3>Menu</h3>
      <ul>
        <li>
          <div class="menu-1-level"><a href="/~wroblek1/index.html">Strona główna</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 1</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/frame.html">Ramki</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/iframe.html">Pływająca ramka</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.html">CV (div,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.html">CV (table,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.xhtml">CV (div,xhtml)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.xhtml">CV(table,xhtml)</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 2</a>
          </div>         
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/table.html">Dynamiczna tabela</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/form.html">Formularz</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 3</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/index.php">Strona główna</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/form.php">Formularz</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 4</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 5</a>
          </div>
        </li>
      </ul>
    </div>
    <div id="content">
      <div class="entry">
        <div class="entry-header">
          <h4>Logowanie</h4>
        </div>
        <form action="" method="POST">
        <div class="entry-content no-indent">
          <div class="row">
              <label for="login">Login: </label>
              <input type="text" id="login" name="login" maxlength="20">
            </div>

            <div class="row">
              <label for="pass">Hasło: </label>
              <input type="password" id="pass" name="pass" maxlength="20">
            </div>

            <div class="center">
              <input type="submit" id="submit" value="Wyślij">
            </div>
         </div>
         </form>
      </div>

    </div>

    <div id="footer">
      <div id="page-source">
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p3/login.php">Źródło strony</a>
        <a> | </a>
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
      </div>
      <div id="copyright">&copy; Krzysztof Wróblewski</div>
      <div id="logo-w3">
        <a href="http://validator.w3.org/check?uri=referer">
          <img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="31" width="88">
        </a>
        <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wroblek1%2Fstyle.css">
          <img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Poprawny CSS!">
        </a>
      </div>
    </div>
  </div>
</body>

</html>
