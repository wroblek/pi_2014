<?php
  include('checklogin.php');

  $useragent=$_SERVER['HTTP_USER_AGENT'];
  if(!isset($_GET['nomobile']))
  if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
    header('Location: indexm.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Strona domowa Krzysztofa Wróblewskiego">
  <meta name="author" content="Krzysztof Wróblewski">
  <meta http-equiv="content-language" content="pl">
  <link rel="stylesheet" type="text/css" href="../style.css">
  <title>Strona domowa Krzysztofa Wrólewskiego</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <div class="center-text">
        <h2>Strona domowa Krzysztofa Wrólewskiego</h2>
      </div>
    </div>
    <div id="menu">
      <h3>Menu</h3>
      <ul>
        <li>
          <div class="menu-1-level"><a href="/~wroblek1/index.html">Strona główna</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 1</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/frame.html">Ramki</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/iframe.html">Pływająca ramka</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.html">CV (div,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.html">CV (table,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.xhtml">CV (div,xhtml)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.xhtml">CV(table,xhtml)</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 2</a>
          </div>         
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/table.html">Dynamiczna tabela</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/form.html">Formularz</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 3</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/index.php">Strona główna</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/form.php">Formularz</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/logout.php">Wyloguj</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 4</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 5</a>
          </div>
        </li>
      </ul>
    </div>
    <div id="content">
      <div class="entry">
        <div class="entry-header">
          <h4>Wpis 1</h4>
        </div>
        <div class="entry-content">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquam arcu sit amet lectus feugiat, non molestie leo dictum. Maecenas et elementum massa. Vivamus suscipit turpis nunc, id suscipit ex venenatis sed. Integer rhoncus, augue
            id consequat molestie, neque sapien sagittis metus, non rutrum sem ex scelerisque tellus. Integer massa ex, scelerisque ac cursus nec, semper et libero. Nunc sit amet urna sed risus sagittis ornare non in eros. Fusce sollicitudin leo ut libero
            gravida, a condimentum nulla mattis. Nullam quis porta turpis, id elementum metus.</p>
          <p>Quisque et mauris ut orci hendrerit porta in vel leo. Morbi suscipit congue justo. Nulla facilisis rutrum mattis. Vivamus et ipsum id urna malesuada lacinia quis ut neque. Morbi et vehicula lacus. Duis faucibus imperdiet elit vel porttitor.
            Quisque nec felis non lorem auctor molestie vitae ac sapien. Proin ultricies accumsan purus imperdiet condimentum. Praesent commodo nisl a suscipit faucibus.</p>
        </div>
      </div>

    </div>

    <div id="footer">
      <div id="page-source">
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p3/index.html">Źródło strony</a>
        <a> | </a>
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
      </div>
      <div id="copyright">&copy; Krzysztof Wróblewski</div>
      <div id="logo-w3">
        <a href="http://validator.w3.org/check?uri=referer">
          <img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="31" width="88">
        </a>
        <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wroblek1%2Fstyle.css">
          <img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Poprawny CSS!">
        </a>
      </div>
    </div>
  </div>
</body>

</html>
