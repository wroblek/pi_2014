<?php include('header.php'); ?>
<script type="text/javascript" src="syntax/scripts/shCore.js"></script>
<script type="text/javascript" src="syntax/scripts/shBrushPhp.js"></script>
<script type="text/javascript">SyntaxHighlighter.all();</script>

<div class="entry">
  <div class="entry-header">
    <h4>download.php</h4>
  </div>
  <div class="entry-content">
    <pre class="brush: php">
<!?php
$file = $_GET['p'];

if (file_exists('uploads/'.$file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize('uploads/'.$file));
    readfile('uploads/'.$file);
    exit;
}
?>
    </pre>
  </div>
</div>

<div class="entry">
  <div class="entry-header">
    <h4>filelist.php</h4>
  </div>
  <div class="entry-content">
    <pre class="brush: php">
<!?php 
  $dir = 'uploads/';
  $files = scandir($dir);
  $finfo = finfo_open(FILEINFO_MIME_TYPE);
  $sz = 'BKMGTP';

  foreach ($files as $id => $file) {
    if($id < 2) continue;
    echo '<\tr>';
    echo '<\td>'.($id-1).'<\/td>';
    echo '<\td>\<\a href="download?p='.$file.'">'.$file.'\<\/a><\/td>';
    echo '<\td>'.finfo_file($finfo, $dir.$file).'\<\/td>';
    $factor = floor((strlen(filesize($dir.$file)) - 1) / 3);
     
    echo '<\td>'.sprintf("%.2f", filesize($dir.$file) / pow(1024, $factor)) . @$sz[$factor].'<\/td>';
    echo '<\/tr>';
  }
  finfo_close($finfo);
?>
    </pre>
  </div>
</div>

<div class="entry">
  <div class="entry-header">
    <h4>object.php</h4>
  </div>
  <div class="entry-content">
    <pre class="brush: php">
<!?php
  class Osoba {
    var $imie;
    var $nazwisko;
    var $pesel;

    function __construct($i, $n, $p) {
      $this->imie = $i;
      $this->nazwisko = $n;
      $this->pesel = $p;
    }

    function __destruct() {

    }

    function przedstaw(){
      echo 'Imię: '.$this->imie.'; Nazwisko: '.$this->nazwisko.'; PESEL: '.$this->pesel;
    }
  }

  class Student extends Osoba {
    var $ocena;

    function __construct($i, $n, $p, $o) {
      $this->imie = $i;
      $this->nazwisko = $n;
      $this->pesel = $p;
      $this->ocena = $o;
    }
    function przedstaw(){
      echo 'Imię: '.$this->imie.'; Nazwisko: '.$this->nazwisko.'; PESEL: '.$this->pesel.'; Ocena: '.$this->ocena;
    }
  }

  $os = new Osoba("Kacper", "Kowalski", "82072613405");
  $st = new Osoba("Szymon", "Wiśniewski", "67091803079", 4);
?>
    </pre>
    <pre class="brush: php">
<!?php $os->przedstaw(); ?>
<br>
<!?php $st->przedstaw(); ?>
    </pre>
    <pre class="brush: php">
<!?php
  $oss = serialize($os); 
  var_dump($oss);
  var_dump(unserialize($oss));
  $sts = serialize($st); 
  var_dump($sts);
  var_dump(unserialize($sts));
?>
    </pre>
  </div>
</div>

<div class="entry">
  <div class="entry-header">
    <h4>upload1.php</h4>
  </div>
  <div class="entry-content">
    <pre class="brush: php">
<!?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["file1"]["name"]);
$uploadOk = 1;

if (file_exists($target_file)) {
    $uploadOk = 0;
}
if ($_FILES["file1"]["size"] > 500000) {
    $uploadOk = 0;
}
if ($uploadOk == 0) {
} else {
    move_uploaded_file($_FILES["file1"]["tmp_name"], $target_file);
}
header("Location: filelist.php");
?>
    </pre>
  </div>
</div>

<div class="entry">
  <div class="entry-header">
    <h4>file3.php</h4>
  </div>
  <div class="entry-content">
    <pre class="brush: php">
<!?php
$target_dir = "uploads/";
$target_file1 = $target_dir . basename($_FILES["file1"]["name"]);
$target_file2 = $target_dir . basename($_FILES["file2"]["name"]);
$target_file3 = $target_dir . basename($_FILES["file3"]["name"]);
$uploadOk = 1;

if (file_exists($target_file1)) {
    $uploadOk = 0;
}
if (file_exists($target_file2)) {
    $uploadOk = 0;
}
if (file_exists($target_file3)) {
    $uploadOk = 0;
}

if ($_FILES["file1"]["size"] > 500000) {
    $uploadOk = 0;
}
if ($_FILES["file2"]["size"] > 500000) {
    $uploadOk = 0;
}
if ($_FILES["file3"]["size"] > 500000) {
    $uploadOk = 0;
}

if ($uploadOk == 0) {
} else {
    move_uploaded_file($_FILES["file1"]["tmp_name"], $target_file1);
    move_uploaded_file($_FILES["file2"]["tmp_name"], $target_file2);
    move_uploaded_file($_FILES["file3"]["tmp_name"], $target_file3);
}
header("Location: filelist.php");
?>
    </pre>
  </div>
</div>


<div class="entry">
  <div class="entry-header">
    <h4>responser.php</h4>
  </div>
  <div class="entry-content">
    <pre class="brush: php">
<!?php 

  if(isset($_GET['name']))
    return 'Witaj '.$_GET['name'];
  if(isset($_POST['name']))
    return 'Witaj '.$_POST['name'];
?>
    </pre>
  </div>
</div>


<div id="page-source">
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/code.php">Źródło strony</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
</div>
<?php include('footer.php'); ?>