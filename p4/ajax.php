<?php include('header.php'); ?>
<div class="entry">
  <div class="entry-header">
    <h4>AJAX</h4>
  </div>
  <div class="entry-content">
    <label for="name">Wpisz imię: </label>
    <input type="text" id="name" name="name"><br><br>
    <button onclick="return getSync();">GET Sync</button>
    <button onclick="return getAsync();">GET Async</button>
    <button onclick="return postSync();">POST Sync</button>
    <button onclick="return postAsync();">POST Async</button>
    <div id="result">
    </div>
    <div id="animation">
      <div id="rect"></div>
    </div>

    <script type="text/javascript" src="ajax.js"></script>
  </div>
</div>

<div id="page-source">
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/ajax.php">Źródło strony</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/ajax.js">Źródło skryptu</a>
</div>
<?php include('footer.php'); ?>