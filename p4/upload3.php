<?php
$target_dir = "uploads/";
$target_file1 = $target_dir . basename($_FILES["file1"]["name"]);
$target_file2 = $target_dir . basename($_FILES["file2"]["name"]);
$target_file3 = $target_dir . basename($_FILES["file3"]["name"]);
$uploadOk = 1;

if (file_exists($target_file1)) {
    $uploadOk = 0;
}
if (file_exists($target_file2)) {
    $uploadOk = 0;
}
if (file_exists($target_file3)) {
    $uploadOk = 0;
}

if ($_FILES["file1"]["size"] > 500000) {
    $uploadOk = 0;
}
if ($_FILES["file2"]["size"] > 500000) {
    $uploadOk = 0;
}
if ($_FILES["file3"]["size"] > 500000) {
    $uploadOk = 0;
}

if ($uploadOk == 0) {
} else {
    move_uploaded_file($_FILES["file1"]["tmp_name"], $target_file1);
    move_uploaded_file($_FILES["file2"]["tmp_name"], $target_file2);
    move_uploaded_file($_FILES["file3"]["tmp_name"], $target_file3);
}
header("Location: filelist.php");
?>