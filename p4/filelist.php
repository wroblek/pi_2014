<?php include('header.php'); ?>
<div class="entry">
  <div class="entry-header">
    <h4>Lista plików</h4>
  </div>
  <div class="entry-content">
    <table>
      <thead>
        <tr>
          <th>Lp.</th><th>Nazwa</th><th>Typ</th><th>Rozmiar</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          $dir = 'uploads/';
          $files = scandir($dir);
          $finfo = finfo_open(FILEINFO_MIME_TYPE);
          $sz = 'BKMGTP';

          foreach ($files as $id => $file) {
            if($id<2) continue;
            echo '<tr>';
            echo '<td>'.($id-1).'</td>';
            echo '<td><a href="download?p='.$file.'">'.$file.'</a></td>';
            echo '<td>'.finfo_file($finfo, $dir.$file).'</td>';
            $factor = floor((strlen(filesize($dir.$file)) - 1) / 3);
             
            echo '<td>'.sprintf("%.2f", filesize($dir.$file) / pow(1024, $factor)) . @$sz[$factor].'</td>';
            //echo '<td>'.filesize($dir.$file).'</td>';
            echo '</tr>';
          }
          finfo_close($finfo);
        ?>
      </tbody>
    </table>
  </div>
</div>

<div id="page-source">
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/filelist.php">Źródło strony</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
</div>
<?php include('footer.php'); ?>