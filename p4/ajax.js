function getSync() {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/~wroblek1/p4/responser.php?name='+document.getElementById("name").value, false);  // `false` makes the request synchronous
  xhr.send(null);

  if (xhr.status === 200) {
    document.getElementById("result").innerHTML = xhr.responseText;
  }
}

function getAsync() {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', '/~wroblek1/p4/responser.php?name='+document.getElementById("name").value, true);
  xhr.onload = function (e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        document.getElementById("result").innerHTML = xhr.responseText;
      }
    }
  };
  xhr.send(null);
}

function postSync() {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/~wroblek1/p4/responser.php', false);  // `false` makes the request synchronous
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xhr.send("name="+document.getElementById("name").value);

  if (xhr.status === 200) {
    document.getElementById("result").innerHTML = xhr.responseText;
  }
}

function postAsync() {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/~wroblek1/p4/responser.php', true);
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xhr.onload = function (e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        document.getElementById("result").innerHTML = xhr.responseText;
      }
    }
  };
  xhr.send("name="+document.getElementById("name").value);
}

function move(elem) {
  var left = 0;
  function frame() {    
    left++;    
    elem.style.left = left + 'px';
    if (left >= (elem.parentElement.offsetWidth-elem.offsetWidth))
      elem.style.left = left = 0;
  }
  var id = setInterval(frame, 10);
}

move(document.getElementById("rect"));
