<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Strona domowa Krzysztofa Wróblewskiego">
  <meta name="author" content="Krzysztof Wróblewski">
  <meta http-equiv="content-language" content="pl">
  <link rel="stylesheet" type="text/css" href="/~wroblek1/style.css">
  <link type="text/css" rel="stylesheet" href="syntax/styles/shCoreDefault.css">
  <title>Strona domowa Krzysztofa Wrólewskiego</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <div class="center-text">
        <h2>Strona domowa Krzysztofa Wrólewskiego</h2>
      </div>
    </div>
    <div id="menu">
      <h3>Menu</h3>
      <ul>
        <li>
          <div class="menu-1-level"><a href="/~wroblek1/index.html">Strona główna</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 1</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/frame.html">Ramki</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/iframe.html">Pływająca ramka</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.html">CV (div,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.html">CV (table,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.xhtml">CV (div,xhtml)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.xhtml">CV(table,xhtml)</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 2</a>
          </div>         
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/table.html">Dynamiczna tabela</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/form.html">Formularz</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 3</a>
          </div>    
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/index.php">Strona główna</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/form.php">Formularz</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 4</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/upload.php">Upload</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/filelist.php">Lista plików</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/object.php">Obiekty</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/ajax.php">AJAX</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/db.php">Baza użytkowników</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 5</a>
          </div>
        </li>
      </ul>
    </div>
    <div id="content">