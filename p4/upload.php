<?php include('header.php'); ?>
<div class="entry">
  <div class="entry-header">
    <h4>Upload jednego pliku</h4>
  </div>
  <div class="entry-content">
    <form method="POST" action="upload1.php" enctype="multipart/form-data">
      <div>
        <input type="file" name="file1"><br>
        <input type="submit" value="Wyślij" name="submit">
      </div>
    </form>
  </div>
</div>
<div class="entry">
  <div class="entry-header">
    <h4>Upload 3 plików</h4>
  </div>
  <div class="entry-content">
    <form method="POST" action="upload3.php" enctype="multipart/form-data">
      <div>
        <input type="file" name="file1"><br>
        <input type="file" name="file2"><br>
        <input type="file" name="file3"><br>
        <input type="submit" value="Wyślij" name="submit">
      </div>
    </form>
  </div>
</div>


<div id="page-source">
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/upload.php">Źródło strony</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
</div>
<?php include('footer.php'); ?>