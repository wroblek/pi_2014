<?php include('header.php'); ?>
<div class="entry">
  <div class="entry-header">
    <h4>Baza użytkowników</h4>
  </div>
  <div class="entry-content">
    <form action="" method="GET">
      <div>
        <label for="login">Login</label>
        <input type="text" name="login" id="login" size="10" onkeyup="filterList(this);">
        <input type="hidden" name="Imie" id="name">
        <input type="hidden" name="Nazwisko" id="lastname">
      </div>
    </form>
    <div>
      <table id="list">
        <thead>
          <tr>
            <th>Imię</th>
            <th>Nazwisko</th>
            <th>Login</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script src="db.js" type="text/javascript"></script>

<div id="page-source">
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/db.php">Źródło strony</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/db.js">Źródło skryptu</a>
</div>
<?php include('footer.php'); ?>