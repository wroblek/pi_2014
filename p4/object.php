<?php
  class Osoba {
    var $imie;
    var $nazwisko;
    var $pesel;

    function __construct($i, $n, $p) {
      $this->imie = $i;
      $this->nazwisko = $n;
      $this->pesel = $p;
    }

    function __destruct() {

    }

    function przedstaw(){
      echo 'Imię: '.$this->imie.'; Nazwisko: '.$this->nazwisko.'; PESEL: '.$this->pesel;
    }
  }

  class Student extends Osoba {
    var $ocena;

    function __construct($i, $n, $p, $o) {
      $this->imie = $i;
      $this->nazwisko = $n;
      $this->pesel = $p;
      $this->ocena = $o;
    }
    function przedstaw(){
      echo 'Imię: '.$this->imie.'; Nazwisko: '.$this->nazwisko.'; PESEL: '.$this->pesel.'; Ocena: '.$this->ocena;
    }
  }

  $os = new Osoba("Kacper", "Kowalski", "82072613405");
  $st = new Osoba("Szymon", "Wiśniewski", "67091803079", 4);
?>

<?php include('header.php'); ?>
<div class="entry">
  <div class="entry-header">
    <h4>Obiekty</h4>
  </div>
  <div class="entry-content">
    <?php $os->przedstaw(); ?>
    <br>
    <?php $st->przedstaw(); ?>
  </div>
</div>

<div class="entry">
  <div class="entry-header">
    <h4>Serializacja/Deserializacja</h4>
  </div>
  <div class="entry-content">
    <?php $oss = serialize($os); 
      var_dump($oss);
    ?>
    <br>
    <br>
    <?php
      var_dump(unserialize($oss));
    ?>
    <br>
    <br>
    <br>
    <?php $sts = serialize($st); 
      var_dump($sts);
    ?>
    <br>
    <br>
    <?php
      var_dump(unserialize($sts));
    ?>
  </div>
</div>


<div id="page-source">
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p4/object.php">Źródło strony</a>
  <a> | </a>
  <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/style.css">Źródło styli</a>
</div>
<?php include('footer.php'); ?>