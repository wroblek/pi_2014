/**
 * Created by Krzysztof on 2014-11-05.
 */
var addButton = document.createElement("button");
addButton.innerText = "Dodaj kolumnę";
var cellInput = document.createElement("input");
cellInput.className = "cell-input";
cellInput.type = "text";
cellInput.size = 10;

var btnCurrentCell;

var genTable = function () {
    var names = ["Jakub","Kacper","Szymon","Mateusz","Filip","Michał","Bartosz","Wiktor","Piotr","Dawid","Adam","Maciej","Jan","Igor","Mikołaj","Patryk","Paweł","Dominik","Oskar","Antoni","Wojciech","Kamil","Aleksander","Krzysztof","Oliwier","Marcel","Karol","Franciszek","Tomasz","Maksymilian","Hubert","Bartłomiej","Adrian","Alan","Sebastian","Miłosz","Krystian","Łukasz","Nikodem","Gabriel","Marcin","Stanisław","Damian","Konrad","Daniel","Fabian","Błażej","Rafał","Tymoteusz","Ksawery"];
    var lastNames = ["Nowak","Kowalski","Wiśniewski","Wójcik","Kowalczyk","Kamiński","Lewandowski","Zieliński","Woźniak","Szymański","Dąbrowski","Kozłowski","Jankowski","Mazur","Kwiatkowski","Wojciechowski","Krawczyk","Kaczmarek","Piotrowski","Grabowski","Zając","Pawłowski","Król","Michalski","Wróbel","Wieczorek","Jabłoński","Nowakowski","Majewski","Olszewski","Stępień","Dudek","Jaworski","Malinowski","Adamczyk","Pawlak","Górski","Nowicki","Sikora","Witkowski","Walczak","Rutkowski","Baran","Michalak","Szewczyk","Ostrowski","Tomaszewski","Zalewski","Wróblewski","Pietrzak"];

    var rowToGenerate = parseInt(document.getElementById("to-generate").value,10);
    var table = document.createElement("table");
    table.id = "table";
    var thead = document.createElement("thead");

    var row = document.createElement("tr");
    var th = document.createElement("th");
    th.innerText = "L.p.";
    row.appendChild(th);
    th = document.createElement("th");
    th.innerText = "Imię";
    row.appendChild(th);
    th = document.createElement("th");
    th.innerText = "Nazwisko";
    row.appendChild(th);
    th = document.createElement("th");
    th.appendChild(addButton);
    btnCurrentCell = th;
    row.appendChild(th);

    thead.appendChild(row);
    table.appendChild(thead);

    var tbody = document.createElement("tbody");
    table.appendChild(tbody);

    for(var i = 0; i < rowToGenerate; i++){
        row = document.createElement("tr");
        tbody.appendChild(row);
        var newCell = tbody.rows[i].insertCell(-1);
        newCell.innerText = (i+1);
        newCell = tbody.rows[i].insertCell(-1);
        newCell.innerText = names[Math.floor(Math.random()*names.length)];
        newCell = tbody.rows[i].insertCell(-1);
        newCell.innerText = lastNames[Math.floor(Math.random()*lastNames.length)];
        newCell = tbody.rows[i].insertCell(-1);
        newCell.appendChild(cellInput.cloneNode(true));
    }


    document.getElementById("list").appendChild(table);
    document.getElementById("generate").innerHTML = '';
};

function addColumn()
{
    var tableHead = document.getElementById("table").tHead;
    btnCurrentCell.removeChild(addButton);
    var newTH = document.createElement('th');
    newTH.appendChild(addButton);
    tableHead.rows[0].appendChild(newTH);
    btnCurrentCell = newTH;


    var tableBody = document.getElementById("table").tBodies[0];
    for (var i = 0; i < tableBody.rows.length; i++) {
        var newCell = tableBody.rows[i].insertCell(-1);
        newCell.appendChild(cellInput.cloneNode(true));
    }
}


addButton.onclick = addColumn;
