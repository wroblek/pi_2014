<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Strona domowa Krzysztofa Wróblewskiego">
  <meta name="author" content="Krzysztof Wróblewski">
  <meta http-equiv="content-language" content="pl">
  <link rel="stylesheet" type="text/css" href="/~wroblek1/style.css">
  <link type="text/css" rel="stylesheet" href="style.css">
  <title>Strona domowa Krzysztofa Wrólewskiego</title>
</head>

<body>
  <div id="container">
    <div id="header">
      <div class="center-text">
        <h2>Strona domowa Krzysztofa Wrólewskiego</h2>
      </div>
    </div>
    <div id="menu">
      <h3>Menu</h3>
      <ul>
        <li>
          <div class="menu-1-level"><a href="/~wroblek1/index.html">Strona główna</a>
          </div>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 1</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/frame.html">Ramki</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p1/iframe.html">Pływająca ramka</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.html">CV (div,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.html">CV (table,html)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_div.xhtml">CV (div,xhtml)</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/cv/index_table.xhtml">CV(table,xhtml)</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 2</a>
          </div>         
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/table.html">Dynamiczna tabela</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p2/form.html">Formularz</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 3</a>
          </div>    
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/index.php">Strona główna</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/form.php">Formularz</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p3/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 4</a>
          </div>
          <ul>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/upload.php">Upload</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/filelist.php">Lista plików</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/object.php">Obiekty</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/ajax.php">AJAX</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/db.php">Baza użytkowników</a>
              </div>
            </li>
            <li>
              <div class="menu-2-level"><a href="/~wroblek1/p4/code.php">Kod źródłowy PHP</a>
              </div>
            </li>
          </ul>
        </li>
        <li class="sep">&nbsp;</li>
        <li>
          <div class="menu-1-level"><a>Projekt 5</a>
          </div>
        </li>
      </ul>
    </div>
    <div id="content">
      <div class="entry">
        <div class="entry-header">
          <h3>Mapa</h3>
        </div>
        <div class="entry-content">
          <div id="map-canvas"></div>
        </div>
      </div>

      <div class="entry hidden" id="elevation-entry">
        <div class="entry-header">
          <h3>Profil terenu trasy</h3>
        </div>
        <div class="entry-content">
          <div id="elevation" onmouseout="clearMouseMarker()"></div>
        </div>
      </div>


      <div class="entry">
        <div class="entry-header">
          <h3>Rodzaj transportu</h3>
        </div>
        <div class="entry-content">
          <div class="darker-bg"><input type="radio" name="transport" value="DRIVING" checked="checked" onchange="listMarkers();">Samochód</div>
          <div class="darker-bg"><input type="radio" name="transport" value="BICYCLING" onchange="listMarkers();">Rower</div>
          <div class="darker-bg"><input type="radio" name="transport" value="TRANSIT" onchange="listMarkers();">Komunikacja publiczna</div>
          <div class="darker-bg"><input type="radio" name="transport" value="WALKING" onchange="listMarkers();">Pieszo</div>
        </div>
      </div>

      <div class="entry">
        <div class="entry-header">
          <h3>Punkty trasy</h3>
        </div>
        <div class="entry-content">
          <div id="markers"></div>
        </div>
      </div>

      <div class="hidden" id="directions-entry">
        <div class="entry-header">
          <h3>Wskazówki dojazdu</h3>
        </div>
        <div class="entry-content">
          <div id="directions"></div>
        </div>
      </div>

      <div class="hidden" id="summary-entry">
        <div class="entry-header">
          <h3>Podsumowanie trasy</h3>
        </div>
        <div class="entry-content">
          <div id="summary"></div>
        </div>
      </div>


    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDALZpZfTjx_TSYUUgZX4oJcQ8tjM06VBI"></script>
    <script type="text/javascript" src="map.js"></script>

  </div>

    <div id="footer">
      <div id="page-source">
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p5/index.php">Źródło strony</a>
        <a> | </a>
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p5/style.css">Źródło styli</a>
        <a> | </a>
        <a href="view-source:http://volt.iem.pw.edu.pl/~wroblek1/p5/map.js">Źródło skryptu</a>
      </div>
      <div id="copyright">&copy; Krzysztof Wróblewski</div>
      <div id="logo-w3">
        <a href="http://validator.w3.org/check?uri=referer">
          <img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="31" width="88">
        </a>
        <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wroblek1%2Fp5%2Fstyle.css">
          <img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Poprawny CSS!">
        </a>
      </div>
    </div>
  </div>
</body>

</html>
