var markers = [];
var map;
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
var elevator = new google.maps.ElevationService();
var chart;
var mapCenter;
var mapZoomzoom;
var mousemarker = null;
var elevations = null;
google.load('visualization', '1', {packages: ['corechart']});
google.setOnLoadCallback(function() {
    chart = new google.visualization.ColumnChart(document.getElementById('elevation'));
      google.visualization.events.addListener(chart, 'onmouseover', function(e) {
        if (mousemarker == null) {
          mousemarker = new google.maps.Marker({
          position: elevations[e.row].location,
          map: map,
          icon: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
        });
      } else {
        mousemarker.setPosition(elevations[e.row].location);
      }
    });
});


function initialize() {
  var mapOptions = {
    center: { lat: 52.2182433, lng: 20.9967693},
    zoom: 13,
    mapTypeControl: false,
    streetViewControl: false,
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  mapCenter = map.getCenter();
  mapZoom = map.getZoom();

  var resetControlDiv = document.createElement('div');
  var resetControl = new ResetControl(resetControlDiv, map);

  resetControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(resetControlDiv);

  google.maps.event.addListener(map, 'click', function(event) {
     placeMarker(event.latLng);
  });

  function placeMarker(location) {
    if(markers.length < 7){
      var marker = new google.maps.Marker({
          position: location, 
          map: map,
          draggable: true,
      });
      markers.push(marker);
  
      marker.info = new google.maps.InfoWindow({
        content: '',
      });
      marker.infoshown = false;

      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({'latLng': location}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            marker.info.setContent("<div>"+results[0].formatted_address+"</div>");
          }
        }

        listMarkers();
      });
  
      google.maps.event.addListener(marker, 'click', function (event) {
        if(marker.infoshown){
          marker.info.close();
          marker.infoshown = false;
        } else {
          marker.info.open(map, marker);
          marker.infoshown = true;
        }
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        marker.setPosition(event.latLng); 
        geocoder.geocode({'latLng': event.latLng}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              marker.info.setContent(results[1].formatted_address);
            }
          } else 
            marker.info.setContent('');
        });
        listMarkers();
      });

    }
  }
}

function listMarkers() {
  var d = document.getElementById("markers");
  d.innerHTML = '';
  for(var i in markers){
    var row = document.createElement("div");

    var buttons = document.createElement("div");
    buttons.className = "button-box";

    var up = document.createElement("button");
    up.onclick = moveMarkerUp(i);
    up.className = "left";
    up.innerHTML = "^";
    if(i>0)
     buttons.appendChild(up);

    var down = document.createElement("button");
    down.onclick = moveMarkerDown(i);
    down.className = "right";
    down.innerHTML = "v";
    if(i<6)
      buttons.appendChild(down);

    row.appendChild(buttons);
    var s = document.createElement("span");
    row.id = 'marker-'+i;
    s.innerHTML = markers[i].info.getContent();
    row.appendChild(s);

    var rem = document.createElement("button");
    rem.onclick = removeMarker(i);
    rem.className = "btn-rem";
    rem.innerHTML = "x";
    row.appendChild(rem);
    d.appendChild(row);
  }

  if(markers.length > 1) {
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById("directions"));
    var waypoints = [];
    for(var i = 1; i<markers.length-1;i++){
      waypoints.push({
        location: markers[i].getPosition(),
        stopover: true
      });
    }

    var radios = document.getElementsByName('transport');
    var transport = google.maps.TravelMode.DRIVING;
    for (var i = 0, length = radios.length; i < length; i++) {
      if (radios[i].checked) {
          if(radios[i].value === "DRIVING")
            transport = google.maps.TravelMode.DRIVING;
          else if(radios[i].value === "BICYCLING")
            transport = google.maps.TravelMode.BICYCLING;
          else if(radios[i].value === "TRANSIT")
            transport = google.maps.TravelMode.TRANSIT;
          else
            transport = google.maps.TravelMode.WALKING;
          break;
      }
    }

    var request = {
      origin: markers[0].getPosition(),
      destination: markers[markers.length-1].getPosition(),
      waypoints: waypoints,
      provideRouteAlternatives: false,
      travelMode: transport,
      unitSystem: google.maps.UnitSystem.METRIC
    };

    directionsService.route(request, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(result);
        document.getElementById("directions-entry").className = "entry";
        var pathRequest = {
          'path': result.routes[0].overview_path,
          'samples': 256
        }
        elevator.getElevationAlongPath(pathRequest, function (results,status) {
          if (status != google.maps.ElevationStatus.OK) {
            return;
          }
          elevations = results;
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Sample');
          data.addColumn('number', 'Elevation');
          for (var i = 0; i < results.length; i++) {
            data.addRow(['', elevations[i].elevation]);
          }
          document.getElementById("elevation-entry").className = "entry";
          chart.draw(data, {
            height: 150,
            legend: 'none',
            titleY: 'Wysokość terenu (m)',
            bar: {
              groupWidth: '100%'
            }
          });
        });

        var totalDistance = 0;
        var totalTime = 0;
        for(var x in result.routes[0].legs){
          totalDistance += result.routes[0].legs[x].distance.value;
          totalTime += result.routes[0].legs[x].duration.value;
        }
        var textTotDisplay = "";
        textTotDisplay += "Długość trasy: " + (totalDistance/1000).toFixed(1) + " km";
        textTotDisplay += "<br>";
        if((totalTime/60)%1 === 0)
          textTotDisplay += "Czas całkowity: " + (totalTime/60) + " min";
        else 
          textTotDisplay += "Czas całkowity: około " + Math.round((totalTime/60)) + " min"; 
        
        document.getElementById("summary").innerHTML = textTotDisplay;    
        document.getElementById("summary-entry").className = "entry";
        setTimeout(function () {
          mapCenter = map.getCenter();
          mapZoom = map.getZoom();     
        }, 200);
      }
    });
  
  } else {
    document.getElementById("summary").innerHTML = "";
    directionsDisplay.setPanel(null);
    directionsDisplay.setMap(null);
    document.getElementById("elevation-entry").className = "hidden";
    document.getElementById("directions-entry").className = "hidden";
    document.getElementById("summary-entry").className = "hidden";
    if(chart !== undefined) chart.clearChart();
  }

}

google.maps.event.addDomListener(window, 'load', initialize);

function moveMarkerUp(index) {
    return function() { 
      var newPos = parseInt(index) - 1;
       
      if(index === -1) 
          throw new Error('Element not found in array');
    var elem = markers[index];
       
      if(newPos < 0) 
          newPos = 0;
           
      markers.splice(index,1);
      markers.splice(newPos,0,elem);
      listMarkers(); 
   };
}
function moveMarkerDown(index) {
    return function() { 
       
      var newPos = parseInt(index) + 1;
       
      if(index === -1) 
          throw new Error('Element not found in array');
      var elem = markers[index];
      if(newPos >= markers.length) 
          newPos = markers.length;
      markers.splice(index, 1);
      markers.splice(newPos,0,elem);
      listMarkers(); 
   };
}
function removeMarker(i) {
    return function() { 
      markers[i].setMap(null);
      markers.splice(i,1);
      listMarkers(); };
}

function ResetControl(controlDiv, map) {

  controlDiv.style.padding = '5px';

  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = 'white';
  controlUI.style.borderStyle = 'solid';
  controlUI.style.borderWidth = '2px';
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Resetuj pozycję';
  controlDiv.appendChild(controlUI);

  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Arial,sans-serif';
  controlText.style.fontSize = '12px';
  controlText.style.paddingLeft = '4px';
  controlText.style.paddingRight = '4px';
  controlText.innerHTML = '<strong>Reset</strong>';
  controlUI.appendChild(controlText);

  google.maps.event.addDomListener(controlUI, 'click', function() {
    map.setCenter(mapCenter);
    map.setZoom(mapZoom);
  });
}

function clearMouseMarker() {
  if (mousemarker != null) {
    mousemarker.setMap(null);
    mousemarker = null;
  }
}